const express= require('express');
const server =express();

const swaggerJsDoc= require('swagger-jsdoc');
const swaggerUI= require('swagger-ui-express');

//configuracion middleware para acepta js en su formato

server.use(express.json());
server.use(express.urlencoded({extended: true}));


//configuracion del swagger
const swaggerOptions = {
   swaggerDefinition: {
     info: {
       title: 'Proyecto API',
       version: '1.0.0'
     }
   },
   apis: ['./server.js'],
};

//configurar el swaggerDocs

const swaggerDocs = swaggerJsDoc(swaggerOptions);

//configurar los endpoints....()

server.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

//


//usuarios
const usuarios = [{
    id: 1,
    usuario: "nico_mag",
    nombre_apellido:'administrador',
    telefono: 654321,
    direccion: 'Chacra 13',
    email: 'nicolas@gmail.com',
    pass: '123456',
    admin: true,
    estado: false
  },
  {
    id: 2,
    usuario: "juanpe",
    nombre_apellido:'juan perez',
    telefono: 123,
    direccion: 'Chacra 13',
    email: 'juanP@gmail.com',
    pass: '123456',
    admin: false,
    estado: false
    }    
];

//productos
let productos=[
  {
      id_p: 1,
      nombre: "Bagel de salmon",
      precio: 425
  },
  {
      id_p:2,
      nombre: "Hamburguesa clasica",
      precio: 350
  },
  {
      id_p:3,
      nombre: "Sandwich veggie",
      precio: 310
  },
  {
    id_p:4,
    nombre: "Ensalada veggie",
    precio: 340
  },
  {
    id_p:5,
    nombre: "Focaccia",
    precio: 300
  },
  {
    id_p:6,
    nombre: "Sandwich Focaccia",
    precio: 440
  },
];

// pedidos 
let pedidos=[
  {
    id_pe: 1,
    id_us:1,
    estado: "pendiente",
    nomb:"Bagel de salmon",
    pago:425,  
    metodoPago: "efectivo",  
    dire:"Roca 123",
    detalle:[ 
      {
        id:1,
        cantidad:1
      }
    ],
  }
];

// medios de pago
let pagos=[
  {
    id_mp:1,
    nombre: "Efectivo"
  },
  {
    id_mp:2,
    nombre:"Debito"
  },
  {
    id_mp:3,
    nombre:"Credito"
  }
];

// estados 
let estados=[
  {
    id_e:1,
    nombre: "Confirmado"
  },
  {
    id_e:2,
    nombre:"En preparacion"
  },
  {
    id_e:3,
    nombre:"En camino"
  },
  {
    id_e:4,
    nombre:"Entregado"
  }
]

//mid

const mailDuplicado = (req, res , next) =>{
  let emailDup = req.body.email;

  usuarios.forEach(usuario => {
      if(emailDup == usuario.email){
          res.send ("El email ya existe");
      }
      
  });
  next();
}


const esAdmin = (req, res , next) =>{
  let id_us = req.body.id;

  let flag = false;

  usuarios.forEach(usuario => {
    if(usuario.id == id_us){
      if(usuario.admin == true){
          flag = true;
      }
    }
  });

  if(flag == false){
  res.json('El usuario no es administrador');
  }else{
    next();
  }
};


const estaLogeado = (req, res , next) =>{
  let id_us = req.body.id;

  let flag = false;

  usuarios.forEach(usuario=> {
    if(usuario.id == id_us){
      if(usuario.estado == true){
        flag = true;
      }
    }
  });

  if(flag == false){
  res.json('El usuario no está logeado');
  }else{
    next();
  }
}

const esRequerido =  (req, res , next) =>{
    
  const {nomb_usuario, nombre_apellido, pass, email, telefono, direccion} = req.body;

  usuarios.forEach(usuario => {
      if(nomb_usuario !=="" && nombre_apellido!=="" && pass!=="" && email!=="" && telefono!=="" && direccion!==""){
          next();
      }
      
  });

  res.send("Falta completar campo")

};


//USUARIOS
//alta de usuarios

/**
 * @swagger
 * /usuarios/alta:
 *  post:
 *    description: Crea un nuevo usuario
 *    parameters:
 *    - name: usuario
 *      description: nombre de usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: nombre_apellido
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: Telefono
 *      description: Telefono del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: Direccion
 *      description: Direccion del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: Pass
 *      description: contraseña del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.post('/usuarios/alta',esRequerido,mailDuplicado,(req,res)=>{
  const {nomb_usuario, nombre_apellido, pass, email, telefono, direccion} = req.body;

  const usuario = {
    id: usuarios.length +1,
    usuario: nomb_usuario,
    nombre_apellido: nombre_apellido,
    telefono: telefono,
    direccion: direccion,
    email: email,
    pass: pass,
    admin: false,
    estado: false
  };

  usuarios.push(usuario);
  console.log('Usuario creado exitosamente');
  res.send(usuario);

});

//mostrar usuarios !!!

/**
 * @swagger
 * /usuarios/lista:
 *  get:
 *    description: Devuelve la lista de los usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/usuarios/lista',(req,res)=>{
  res.send(usuarios);
  console.log(usuarios);
})

//Login Usuarios!!!

/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    description: Permite hacer login
 *    parameters:
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: pass del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.post('/usuarios/login',(req,res) =>{
  const {email, pass} = req.body;
  console.log(email,pass);
  let index = -1;
  usuarios.forEach((usuario , i) => {
      if(usuario.email == email){
         if(usuario.pass == pass){
          index = i;
        }
      }
   });
  if(index == -1){
    console.log("error");
    res.send('Los datos no son correctos');
    
  } else{
    console.log(usuarios[index]);
    res.send("Puede acceder")
    usuarios[index].estado = true;
    
  }
});


//PRODUCTOS
//lista de productos !!!

/**
 * @swagger
 * /productos/lista:
 *  get:
 *    description: Devuelve la lista de los productos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/productos/lista',(req,res)=>{
  res.send(productos);
});

//crear producto !!

/**
 * @swagger
 * /productos/alta:
 *  post:
 *    description: Crea un nuevo producto
 *    parameters:
 *    - name: nombre
 *      description: Nombre del nuevo producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio
 *      description: Precio del nuevo producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.post('/productos/alta',esAdmin,(req,res)=>{
  const {nombre, precio} = req.body;

  const producto = {
    id_p: productos.length+1,
    nombre: nombre,
    precio: precio,
  };

  productos.push(producto);
  console.log('Producto creado exitosamente');
  res.send(producto);
});

//modificar producto!! 

/**
 * @swagger
 * /productos/mod:
 *  put:
 *    description: Modifica un producto
 *    parameters:
 *    - name: id_p
 *      description: Id del producto a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del producto 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio
 *      description: Precio del producto 
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.put('/productos/mod',esAdmin,(req,res)=>{
  const {id_p,nombre, precio} = req.body;

  productos.map(prod=>{
    if(prod.id_p == id_p){
      prod.nombre=nombre;
      prod.precio=precio
    }
  })
  console.log('Producto modificado exitosamente');
  res.send(productos);
});

//borrar producto

/**
 * @swagger
 * /productos/del:
 *  delete:
 *    description: Elimina un producto
 *    parameters:
 *    - name: id_p
 *      description: Id del producto a eliminar
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.delete('/productos/del',esAdmin,(req,res)=>{
  const id_p = req.body;
  
  let prod_id = req.body.id_p;

  productos.forEach((producto,i) => {
      if(producto.id_p == prod_id){
          productos.splice(i,1);
      }

  });
  console.log('Producto eliminado exitosamente');
  res.send('Producto eliminado.');

});


//MEDIOS DE PAGO

/**
 * @swagger
 * /mediospago/lista:
 *  get:
 *    description: Devuelve la lista de los medios de pago
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/mediospago/lista', (req,res)=>{
  res.send(pagos);
});

//crear nuevo medio de pago

/**
 * @swagger
 * /mediospago/alta:
 *  post:
 *    description: Crea un nuevo medio de pago
 *    parameters:
 *    - name: nombre
 *      description: nombre del medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.post('/mediospago/alta',esAdmin,(req,res)=>{
  const {nombre} = req.body;

  const pago_nuevo = {
    id_mp: pagos.length+1,
    nombre: nombre
  };

  pagos.push(pago_nuevo);
  console.log('Medio de pago creado exitosamente');
  res.send(pago_nuevo);
});

//editar medio de pago

/**
 * @swagger
 * /mediospago/mod:
 *  put:
 *    description: Modifica los medio de pago
 *    parameters:
 *    - name: id_mp
 *      description: Id del medio de pago a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: nombre del medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.put('/mediospago/mod',esAdmin,(req,res)=>{
  const {id_mp,nombre} = req.body;

  pagos.map(pago=>{
    if(id_mp == pago.id_mp){
      pago.nombre=nombre;
    }
  })
  console.log('Medio de pago modificado exitosamente');
  res.send(pagos);
});

//borrar medios de pago

/**
 * @swagger
 * /mediosPagos/del:
 *  delete:
 *    description: Elimina un medio de pago
 *    parameters:
 *    - name: id_mp
 *      description: Id del medio de pago
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.delete('/mediospago/del',esAdmin,(req,res)=>{
  const id_mp= req.body;
  
  let pag_id = req.body.id_mp;

  pagos.forEach((pago,i) => {
      if(pago.id_mp == pag_id){
          pagos.splice(i,1);
        }
  });
  console.log('medio de pago eliminado exitosamente');
  res.send(pagos);
});


//PEDIDOS
//lista de pedidos

/**
 * @swagger
 * /pedidos/lista:
 *  get:
 *    description: Devuelve la lista de los pedidos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/pedidos/lista',(req,res)=>{
  res.send(pedidos);
})

//crear un nuevo pedido

/** 
* @swagger
 * /pedidos/nuevo:
 *  post:
 *    description: Crear pedido nuevo
 *    consumes:
 *       - application/json
 *    produces:
 *       - application/json
 *    parameters:
 *    - in: body
 *      name: pedido
 *      description: pedido nuevo     
 *      schema:
 *                 type: object
 *                 required:
 *                   - datos
 *                 properties:
 *                   metodo_pago:
 *                     type: integer
 *                   id_us:
 *                     type: integer
 *                   detalle:
 *                     type: array
 *                     items:
 *                        type: object
 *                        properties:
 *                         id:
 *                           type: integer
 *                         cantidad:
 *                           type: integer 
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.post('/pedidos/nuevo',estaLogeado , (req,res)=>{
  const { detalle,id_us,dire,metodo_pago} = req.body;

  let nomb="";
  let monto=0;

  detalle.forEach(detalle=> {
    productos.forEach(prod=>{
      if(prod.id_p==detalle.id){
        nomb = prod.nombre;
        monto= (prod.precio* detalle.cantidad);
      }
    })
  });

  pagos.forEach(pag=>{
  if(metodo_pago==pag.id_mp){
    metodoPago= pag.nombre
  }
  })

  const nuevoPedido={
    id_pe: pedidos.length +1, 
    id_us,
    estado:"pendiente",
    nomb,
    monto,
    metodoPago,
    dire,
    detalle:detalle
    
  }

  pedidos.push(nuevoPedido);
  console.log('Pedido creado exitosamente');
  res.send(nuevoPedido);
});

//historial de pedidos!!!!

/**
 * @swagger
 * /pedidos/historial:
 *  get:
 *    description: Devuelve el historial de los pedidos
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.get('/pedidos/historial',estaLogeado ,(req,res)=>{
 
  const {id} = req.body;
  const historial =[];

  pedidos.forEach(pedido=>{
    if(pedido.id_us == id){
      historial.push(pedido)
    }
  })

  res.send(historial);
  //console.log();

})

//actualizar el pedido !!!

/**
 * @swagger
 * /pedidos/mod:
 *  put:
 *    description: Modifica el estado del pedido
 *    parameters:
 *    - name: id_pe
 *      description: Id del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: ind
 *      description: indice del nuevo estado del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

server.put('/pedidos/mod',esAdmin,(req,res)=>{
 
  const{id_pe,ind}=req.body;
  
  pedidos.map(ped=>{
    if(id_pe ==ped.id_pe){
      ped.estado=estados[ind].nombre
    }
  })
  
  console.log('estado editado');
  res.send(pedidos);
});



server.listen(3000, (req,res)=>{
  console.log('Escuchando en el puerto 3000')
});